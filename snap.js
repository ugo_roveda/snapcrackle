function snapCrackle (maxValue) {
    let result = ""
    let snap = "Snap, "
    let crackle = "Crackle, "
    let snapcrackle = "SnapCrackle, "
    for (let i = 1; i <= maxValue; i++) {
        if (i % 2 === 1 && i % 5 === 0) {
            result += snapcrackle
        } else if (i % 5 === 0) {
            result += crackle
        } else if (i % 2 === 1){
            result += snap
        } else {
            result += i + ', '
        }
    }
    return result
}
console.log(snapCrackle(12))